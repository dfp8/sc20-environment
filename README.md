# SC20 Environment

Environment for SC20 submission for Paper Artifact Description / Article Evaluation (AD/AE) Appendix Submission Form.

Data gathered using: [https://github.com/SC-Tech-Program/Author-Kit/blob/master/collect_environment.sh](https://github.com/SC-Tech-Program/Author-Kit/blob/master/collect_environment.sh) on [Summit](https://docs.olcf.ornl.gov/systems/summit_user_guide.html).